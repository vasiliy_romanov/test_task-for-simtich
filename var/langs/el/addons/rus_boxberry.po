msgid ""
msgstr ""
"Project-Id-Version: cs-cart-latest\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Language-Team: Greek\n"
"Language: el_GR\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: cs-cart-latest\n"
"X-Crowdin-Language: el\n"
"X-Crowdin-File: /release-4.10.3/addons/rus_boxberry.po\n"
"Last-Translator: cscart\n"
"PO-Revision-Date: 2019-07-22 12:06\n"

msgctxt "Addons::name::rus_boxberry"
msgid "Boxberry"
msgstr "Boxberry"

msgctxt "Addons::description::rus_boxberry"
msgid "Shipping method Boxberry"
msgstr "Μέθοδος αποστολής Boxberry"

msgctxt "Languages::rus_boxberry.api_password"
msgid "API token"
msgstr "Αναγνωριστικό API"

msgctxt "Languages::rus_boxberry.take_api_password"
msgid "Boxberry get api key"
msgstr "Boxberry πάρτε τον κωδικό Api"

msgctxt "Languages::rus_boxberry.target_start"
msgid "Boxberry start target code"
msgstr ""

msgctxt "Languages::carrier_rus_boxberry"
msgid "Boxberry"
msgstr "Boxberry"

msgctxt "Languages::rus_boxberry.default_weight"
msgid "Default weight (in grams)"
msgstr "Προεπιλεγμένο βάρος (σε γραμμάρια)"

msgctxt "Languages::rus_boxberry.pickup_point"
msgid "Pick-up point"
msgstr "Σημείο παραλαβής"

msgctxt "Languages::rus_boxberry.select_pickup_point"
msgid "Select pick-up point"
msgstr "Επιλέξτε σημείο παραλαβής"

msgctxt "Languages::rus_boxberry.unknown_error"
msgid "Unknown error"
msgstr "¨Άγνωστο σφάλμα"

msgctxt "Languages::rus_boxberry.connection_error"
msgid "Boxberry server didn't respond"
msgstr ""

msgctxt "Languages::rus_boxberry.enter_api_token"
msgid "Please specify the API token in the shipping method settings."
msgstr "Παρακαλώ ορίστε το κλειδί API στις ρυθμίσεις μεθόδων αποστολής."

msgctxt "Languages::rus_boxberry.not_all_fields_are_filled"
msgid "Not all of the parameters required by the Boxberry API have been specified."
msgstr "Δεν έχουν οριστεί όλες οι παράμετροι που απαιτούνται απο το API του Boxberry."

msgctxt "Languages::rus_boxberry.pickuppoint"
msgid "Pickup point"
msgstr "Σημείο παραλαβής"

msgctxt "Languages::rus_boxberry.status"
msgid "Boxberry status"
msgstr "Κατάσταση Boxberry"

msgctxt "Languages::rus_boxberry.print_label"
msgid "Print label"
msgstr "Ετικέτα Εκτύπωσης"

msgctxt "Languages::rus_boxberry.without_status"
msgid "Without status"
msgstr "Χωρίς κατάσταση"

msgctxt "Languages::rus_boxberry.boxberry_self"
msgid "Delivery to a pickup point"
msgstr "Παράδοση σε ένα από τα σημεία παραλαβής"

msgctxt "Languages::rus_boxberry.boxberry_self_prepaid"
msgid "Delivery to a pickup point (without cash on delivery)"
msgstr "Παράδοση με courier(χωρίς μετρητά κατά την παράδοση)"

msgctxt "Languages::rus_boxberry.boxberry_courier"
msgid "Delivery by courier"
msgstr "Παράδοση με courier"

msgctxt "Languages::rus_boxberry.boxberry_courier_prepaid"
msgid "Delivery by courier (without cash on delivery)"
msgstr "Παράδοση με courier(χωρίς μετρητά κατά την παράδοση)"

msgctxt "Languages::rus_boxberry.addon_was_renamed.title"
msgid "Boxberry: Add-on directory renamed to prevent confusion"
msgstr "Boxberry: Ο κατάλογος του πρόσθετου έχει μετονομαστεί για να αποφευχθεί οποιαδήποτε σύγχυση"

msgctxt "Languages::rus_boxberry.addon_was_renamed.message"
msgid "<p>Boxberry provides its own add-on for integration with [product]. The folder of that add-on is called <b>boxberry</b>. An add-on with the same folder name existed in [product] by default, and it led to conflicts. For that reason, we have renamed the folder of our add-on to <b>rus_boxberry</b>. All Boxberry shipping methods in your store now use our add-on.</p><p>If you have an add-on by Boxberry, you’ll find disabled it on the <b>Add-ons → Manage add-ons</b> page. It has <i>Boxberry</i> listed as the developer under the description.</p><p><a href=\"[boxberry_addon]\" target=\"_blank\" class=\"btn btn-large btn-primary\">View Boxberry add-ons</a></p>"
msgstr ""

